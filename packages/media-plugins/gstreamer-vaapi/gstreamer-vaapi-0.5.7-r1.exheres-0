# Copyright 2010 Paul Seidler <sepek@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Decode MPEG-2, MPEG-4, H.264, VC-1, WMV3 with gstreamer via VA API"
HOMEPAGE="https://github.com/01org/${PN}"
DOWNLOADS="http://www.freedesktop.org/software/vaapi/releases/${PN}/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0.10"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    opengl [[
        description = [ Build OpenGL video output ]
        requires = [ X ]
    ]]
    wayland
    X
"

DEPENDENCIES="
    build:
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
        sys-devel/libtool[>=2.2]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.28]
        media-libs/gstreamer:0.10[>=0.10.36]
        media-plugins/gst-plugins-base:0.10[>=0.10.36]
        media-plugins/gst-plugins-bad:0[>0.10.22]
        sys-apps/systemd [[ description = [ For libudev ] ]]
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/libva[>=1.1.0]
        opengl? (
            x11-dri/mesa
            x11-libs/libva[opengl]
        )
        wayland? (
            sys-libs/wayland[>=1.0.0]
            x11-libs/libva[wayland]
        )
        X? (
            x11-libs/libva[X]
            x11-libs/libX11
            x11-libs/libXrandr
            x11-libs/libXrender
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-drm
    --with-gstreamer-api=0.10
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'
    'opengl glx'
    'wayland'
    'X x11'
)

