# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require test-dbus-daemon meson

SUMMARY="Set of well maintained plugins for gstreamer"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
# TODO (danyspin97): Add X and wayland options
# when gtk3 plugin get respective meson options
# As of version 1.16.0 it uses automagic
MYOPTIONS="
    examples
    gstreamer_plugins:
        aalib      [[ description = [ Output videos as black/white ASCII art ] ]]
        caca       [[ description = [ Colored ASCII art video output using libcaca ] ]]
        cairo      [[ description = [ Cairo-based text overlaying (not suited for subtitles) and video-to-stream rendering ] ]]
        dv         [[ description = [ DV (Digital Video) demuxing and decoding using libdv ] ]]
        dv1394     [[ description = [ Digital IEEE1394 interface video source ] ]]
        flac       [[ description = [ Free Lossless Audio Codec support ] ]]
        gdk-pixbuf [[ description = [ GdkPixbuf-based image decoding, scaling and image output as GdkPixbuf ] ]]
        gtk3       [[ description = [ GTK+3 video sink ] ]]
        jack       [[ description = [ Support for audio input/output via the Jack Audio Connection Kit ] ]]
        lame       [[ description = [ MP3 audio encoding using LAME ] ]]
        mp2        [[ description = [ MP2 audio encoding using TwoLAME ] ]]
        mpg123     [[ description = [ MP3 audio decoding using libmpg123 ] ]]
        oss        [[ description = [ Adds support for OSSv4 ] ]]
        pulseaudio [[ description = [ Audio input, output and mixing using PulseAudio ] ]]
        qt5        [[ description = [ Qt5 Qml GL video sink ] ]]
        shout      [[ description = [ Send data to a SHOUTcast-compatible server (e.g. Icecast) ] ]]
        soup       [[ description = [ HTTP source handling using libsoup ] ]]
        speex      [[ description = [ Speex audio encoding and decoding using the speex library ] ]]
        taglib     [[ description = [ APEv2 and ID3v2 writing using TagLib ] ]]
        v4l        [[ description = [ Capture, radio, and remote controller device support ] ]]
        vpx        [[ description = [ VP8 encoder/decoder support ] ]]
        wavpack    [[ description = [ Support for the lossy/lossless audio format WavPack ] ]]
        X          [[ description = [ X display capturing ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        app-arch/bzip2
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.17]
        gnome-desktop/libgudev[>=147]
        media-libs/gstreamer:1.0[>=${PV}]
        media-libs/libpng:=[>=1.5.1]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        sys-libs/zlib
        examples? ( x11-libs/gtk+:3 )
        gstreamer_plugins:aalib? ( media-libs/aalib )
        gstreamer_plugins:caca? ( media-libs/libcaca )
        gstreamer_plugins:cairo? ( x11-libs/cairo[>=1.10.0] )
        gstreamer_plugins:dv? ( media-libs/libdv[>=0.100] )
        gstreamer_plugins:dv1394? (
            media-libs/libraw1394[>=2.0.0]
            media-libs/libavc1394[>=0.5.4]
            media-libs/libiec61883[>=1.0.0]
        )
        gstreamer_plugins:flac? ( media-libs/flac[>=1.1.4] )
        gstreamer_plugins:gdk-pixbuf? ( x11-libs/gdk-pixbuf:2.0[>=2.8.0] )
        gstreamer_plugins:gtk3? (
            media-plugins/gst-plugins-base[gstreamer_plugins:opengl]
            x11-libs/gtk+:3[>=3.15.0]
        )
        gstreamer_plugins:jack? ( media-sound/jack-audio-connection-kit[>=1.9.7] )
        gstreamer_plugins:lame? ( media-sound/lame )
        gstreamer_plugins:mp2? ( media-libs/twolame[>=0.3.10] )
        gstreamer_plugins:mpg123? ( media-sound/mpg123[>=1.13] )
        gstreamer_plugins:pulseaudio? ( media-sound/pulseaudio[>=2.0] )
        gstreamer_plugins:qt5? (
            x11-libs/qtbase:5[>=5.4]
            x11-libs/qtdeclarative:5[>=5.4]
            x11-libs/qtx11extras:5[>=5.4]
        )
        gstreamer_plugins:shout? ( media-libs/libshout[>=2.0] )
        gstreamer_plugins:soup? ( gnome-desktop/libsoup:2.4[>=2.48.0] )
        gstreamer_plugins:speex? ( media-libs/speex[>=1.1.6] )
        gstreamer_plugins:taglib? ( media-libs/taglib[>=1.5] )
        gstreamer_plugins:wavpack? ( media-sound/wavpack[>=4.60.0] )
        gstreamer_plugins:v4l? ( media-libs/v4l-utils )
        gstreamer_plugins:vpx? ( media-libs/libvpx:=[>=1.4.0] )
        gstreamer_plugins:X? (
            x11-libs/libX11
            x11-libs/libXdamage
            x11-libs/libXext
            x11-libs/libXfixes
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        !media-plugins/gst-plugins-bad:1.0[<1.2.0] [[
            note = [ Maybe some 1.1 versions work too ]
            description = [ Provides DTMF too ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    # core plugins
    -Dalpha=enabled
    -Dapetag=enabled
    -Daudiofx=enabled
    -Daudioparsers=enabled
    -Dauparse=enabled
    -Dautodetect=enabled
    -Davi=enabled
    -Dcutter=enabled
    -Ddebugutils=enabled
    -Ddeinterlace=enabled
    -Ddtmf=enabled
    -Deffectv=enabled
    -Dequalizer=enabled
    -Dflv=enabled
    -Dflx=enabled
    -Dgoom=enabled
    -Dgoom2k1=enabled
    -Dicydemux=enabled
    -Did3demux=enabled
    -Dimagefreeze=enabled
    -Disomp4=enabled
    -Dinterleave=enabled
    -Disomp4=enabled
    -Dlaw=enabled
    -Dlevel=enabled
    -Dmatroska=enabled
    -Dmonoscope=enabled
    -Dmultifile=enabled
    -Dmultipart=enabled
    -Dreplaygain=enabled
    -Drtp=enabled
    -Drtpmanager=enabled
    -Drtsp=enabled
    -Dshapewipe=enabled
    -Dsmpte=enabled
    -Dspectrum=enabled
    -Dudp=enabled
    -Dvideobox=enabled
    -Dvideocrop=enabled
    -Dvideofilter=enabled
    -Dvideomixer=enabled
    -Dwavenc=enabled
    -Dwavparse=enabled
    -Dy4m=enabled

    # (de)compression support
    -Dbz2=enabled

    -Djpeg=enabled
    -Dpng=enabled
    -Dorc=enabled

    # Windows/Solaris/OS X specific
    -Ddirectsound=disabled
    -Dosxaudio=disabled
    -Dosxvideo=disabled
    -Dwaveform=disabled

    -Dnls=enabled

    -Dglib-asserts=disabled
    -Dglib-checks=disabled
    -Dgobject-cast-checks=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'examples'

    # optional plugins
    'gstreamer_plugins:aalib'
    'gstreamer_plugins:caca libcaca'
    'gstreamer_plugins:cairo'
    'gstreamer_plugins:dv'
    'gstreamer_plugins:dv1394'
    'gstreamer_plugins:flac'
    'gstreamer_plugins:gdk-pixbuf'
    'gstreamer_plugins:gtk3'
    'gstreamer_plugins:jack'
    'gstreamer_plugins:lame'
    'gstreamer_plugins:mp2 twolame'
    'gstreamer_plugins:mpg123'
    'gstreamer_plugins:oss'
    'gstreamer_plugins:oss oss4'
    'gstreamer_plugins:pulseaudio pulse'
    'gstreamer_plugins:qt5'
    'gstreamer_plugins:shout shout2'
    'gstreamer_plugins:soup'
    'gstreamer_plugins:speex'
    'gstreamer_plugins:taglib'
    'gstreamer_plugins:v4l v4l2'
    'gstreamer_plugins:v4l v4l2-gudev'
    'gstreamer_plugins:v4l v4l2-libv4l2'
    'gstreamer_plugins:vpx'
    'gstreamer_plugins:wavpack'
    'gstreamer_plugins:X ximagesrc'
    'gstreamer_plugins:X ximagesrc-xshm'
    'gstreamer_plugins:X ximagesrc-xfixes'
    'gstreamer_plugins:X ximagesrc-xdamage'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

src_test() {
    unset DISPLAY

    esandbox allow_net "inet:0.0.0.0@0"
    esandbox allow_net "inet:0.0.0.0@5004"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net "inet:0.0.0.0@0"
    esandbox disallow_net "inet:0.0.0.0@5004"
}
