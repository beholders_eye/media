# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'taglib-1.5.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require github [ tag=v${PV} ] cmake [ api=2 ]

SUMMARY="A library for reading and editing audio meta data"
HOMEPAGE+=" https://${PN}.github.io"

LICENCES="|| ( LGPL-2.1 MPL-1.1 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="doc"

# tests don't get built with BUILD_SHARED_LIBS, last checked: 1.11.1
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+run:
        sys-libs/zlib
    test:
        dev-cpp/cppunit
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/b5115e3497328d80a9accf8c1c303374ec36e884.patch
    "${FILES}"/250c59f783f11e5b23eea4cb73063b6ecb28453e.patch
    "${FILES}"/de87cd7736d52302db330c830adae50b1e667954.patch
    "${FILES}"/14c3ce5737e17671045d70f4eff3bd0fca91ad99.patch
    "${FILES}"/${PN}-1.11.1-CVE-2017-12678.patch
)

src_configure() {
    local conf=(
        -DBUILD_BINDINGS:BOOL=TRUE
        -DBUILD_EXAMPLES:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DBUILD_TESTS=$(expecting_tests && echo ON || echo OFF)
        -DENABLE_CCACHE:BOOL=FALSE
        -DWITH_ASF:BOOL=TRUE
        -DWITH_MP4:BOOL=TRUE
    )
    ecmake "${conf[@]}"
}

src_compile() {
    default

    option doc && emake docs
}

src_install() {
    cmake_src_install

    if option doc ; then
        insinto /usr/share/doc/${PNVR}/
        doins -r doc/*
    fi
}

