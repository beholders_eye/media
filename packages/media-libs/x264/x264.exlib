# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://git.videolan.org/git/${PN}.git"
    require scm-git
else
    MY_PNV="${PN}-snapshot-${PV}-2245-stable"
    DOWNLOADS="https://download.videolan.org/pub/videolan/${PN}/snapshots/${MY_PNV}.tar.bz2"
fi

export_exlib_phases src_configure src_test

SUMMARY="Library for encoding H264/AVC video streams"
HOMEPAGE="https://www.videolan.org/developers/${PN}.html"

LICENCES="GPL-2 GPL-3"
SLOT="0"
MYOPTIONS="
    visualisation [[ description = [ Visualize the encoding process ] ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/nasm[>=2.13] )
        platform:x86? ( dev-lang/nasm[>=2.13] )
    build+run:
        visualisation? ( x11-libs/libX11 )
"

if ! ever is_scm ; then
    WORK=${WORKBASE}/${MY_PNV}
fi

_x264_assembler() {
    case "$(exhost --target)" in
    i686-*|x86_64-*)
        echo nasm
    ;;
    *)
        echo $(exhost --tool-prefix)cc
    ;;
    esac
}

x264_src_configure() {
    export AS=$(_x264_assembler)

    local myconf=(
        # prefixed string executable isn\'t found otherwise
        --cross-prefix=$(exhost --tool-prefix)
        --enable-shared
        --disable-avs
        --disable-ffms
        --disable-gpac
        --disable-lavf
        --disable-opencl
        --disable-swscale
    )

    # --disable-visualize doesn't exist so don't use option_enable
    option visualisation && myconf+=( "--enable-visualize" )

    econf "${myconf[@]}"
}

x264_src_test() {
    :
}

